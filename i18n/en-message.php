<?php

/**
 * English file for plugin mm_core
 *
 * @package Plugins
 * @subpackage mm_core
 *
 * @author Your Name < http://marekmurawski.pl >
 * @version Wolf 0.7.7
 */

return array(
    'Assigned permission <b>:perm</b> to role <b>:role</b>!'
 => 'Assigned permission <b>:perm</b> to role <b>:role</b>!',
    'Could not assign permission <b>:perm</b> to role <b>:role</b>!'
 => 'Could not assign permission <b>:perm</b> to role <b>:role</b>!',
    'Could not create role <b>:role</b>'       => 'Could not create role <b>:role</b>',
    'Created role <b>:role</b>'                => 'Created role <b>:role</b>',
    'Either permission <b>:perm</b> or role <b>:role</b> does not exist!'
 => 'Either permission <b>:perm</b> or role <b>:role</b> does not exist!',
    'Empty response!'                          => 'Empty response!',
    'Execution time'                           => 'Execution time',
    'Imported <b>:count</b> snippets! <br/><b>:names</b>'
 => 'Imported <b>:count</b> snippets! <br/><b>:names</b>',
    'Memory usage'                             => 'Memory usage',
    'Messages'                                 => 'Messages',
    'Permission <b>:perm</b> already exists'   => 'Permission <b>:perm</b> already exists',
    'Permission <b>:perm</b> could not be created'
 => 'Permission <b>:perm</b> could not be created',
    'Permission <b>:perm</b> could not be deleted'
 => 'Permission <b>:perm</b> could not be deleted',
    'Permission <b>:perm</b> created'          => 'Permission <b>:perm</b> created',
    'Permission <b>:perm</b> deleted'          => 'Permission <b>:perm</b> deleted',
    'Permission <b>:perm</b> was not found and not deleted!'
 => 'Permission <b>:perm</b> was not found and not deleted!',
    'Role <b>:role</b> already exists!'        => 'Role <b>:role</b> already exists!',
    'Role <b>:role</b> already has permission <b>:perm</b>!'
 => 'Role <b>:role</b> already has permission <b>:perm</b>!',
    'Role <b>:role</b> could not be deleted'   => 'Role <b>:role</b> could not be deleted',
    'Role <b>:role</b> deleted!'               => 'Role <b>:role</b> deleted!',
    'Role <b>:role</b> has some permissions - cannot delete role with existing permissions'
 => 'Role <b>:role</b> has some permissions - cannot delete role with existing permissions',
    'Role <b>:role</b> was not found and not deleted!'
 => 'Role <b>:role</b> was not found and not deleted!',
    'Sending request...'                       => 'Sending request...',
    'Successfully activated <b>mm_core</b> plugin'
 => 'Successfully activated <b>mm_core</b> plugin',
    'Successfully activated plugin!'           => 'Successfully activated plugin!',
    'Support plugin for mm plugins.'           => 'Support plugin for mm plugins.',
    'mmCore library'                           => 'mmCore library',
);
