<?php

/**
 * Dutch file for plugin mm_core
 *
 * @package Plugins
 * @subpackage mm_core
 *
 * @author Fortron
 * @version Wolf 0.7.7
 */

    return array(
    'Assigned permission <b>:perm</b> to role <b>:role</b>!' => 'Toegewezen toestemming <b>:perm</b> aan rol <b>:rol</b>!',
	'Could not assign permission <b>:perm</b> to role <b>:role</b>!' => 'Kon toestemming :perm </b> niet toewijzen aan rol <b>:rol</b>!',
	'Could not create role <b>:role</b>' => 'Kon rol <b>:role</b> niet aanmaken',
	'Created role <b>:role</b>' => 'Rol <b>:role</b> is aangemaakt',
	'Either permission <b>:perm</b> or role <b>:role</b> does not exist!' => 'Permissie <b>:perm</b> of rol <b>:role</b> bestaat niet!',
	'Imported <b>:count</b> snippets! <br/><b>:names</b>' => 'Er zijn <b>:count</b> snippers ge�mporteerd!<br/><b>:names</b>',
	'Permission <b>:perm</b> already exists' => 'Permissie <b>:perm</b> bestaat al',
	'Permission <b>:perm</b> could not be created' => 'Permissie <b>:perm</b> kan niet worden gemaakt',
	'Permission <b>:perm</b> could not be deleted' => 'Permissie <b>:perm</b> kan niet worden verwijderd',
	'Permission <b>:perm</b> created' => 'Permissie <b>:perm</b> aangemaakt',
	'Permission <b>:perm</b> deleted' => 'Permissie <b>:perm</b> verwijderd',
	'Permission <b>:perm</b> was not found and not deleted!' => 'Permissie <b>:perm</b> is niet gevonden!',
	'Role <b>:role</b> already exists!' => 'Rol <b>:role</b> bestaat al!',
	'Role <b>:role</b> already has permission <b>:perm</b>!' => 'Rol <b>:role</b> heeft al permissie <b>:perm</b>!',
	'Role <b>:role</b> could not be deleted' => 'Rol <b>:role</b> kon niet worden verwijderd',
	'Role <b>:role</b> deleted!' => 'Rol <b>:role</b> verwijderd!',
	'Role <b>:role</b> has some permissions - cannot delete role with existing permissions' => 'Rol <b>:role</b> heeft enkele permissies - kan rol met bestaande permissies niet verwijderen',
	'Role <b>:role</b> was not found and not deleted!' => 'Rol <b>:role</b> is niet gevonden!',
	'Successfully activated <b>mm_core</b> plugin' => 'Plugin <b>mm_core</b> is succesvol geactiveerd',
	'Successfully activated plugin <b>:name</b>' => 'Plugin <b>:name</b> is succesvol geactiveerd',
	'Support plugin for mm plugins.' => 'Ondersteunings plugin voor mm plugins.',
	'mmCore library' => 'mmCore bibliotheek',
    );