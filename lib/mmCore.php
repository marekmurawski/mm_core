<?php

/* Security measure */
if ( !defined('IN_CMS') )
    exit();


class mmCore {

    const VIEW_FOLDER = "../../plugins/mm_core/views/";
    const GLUE        = '<br/>';

    public static $messages = array( );

    public static function callback_mm_core_stylesheet() {
        echo '<link rel="stylesheet" href="' . PLUGINS_URI . 'mm_core/css/mm_core.css" />';

    }


    /**
     *
     * @param type $message
     * @param type $status
     * @param type $arr
     */
    public static function respond($message = '', $status = 'OK', $arr = array( )) {
        // set messages
        $msg_text = (count(self::$messages) > 0) ? implode(self::GLUE, self::$messages) . self::GLUE . $message : $message;
        $default  = array(
                    'message'  => $msg_text,
                    'exe_time' => execution_time(),
                    'mem_used' => memory_usage(),
                    'status'   => $status,
        );

        // add any additional fields
        $response = array_merge($default, $arr);

        echo json_encode($response);
        if ( $status !== 'OK' )
            header("HTTP/1.0 404 Not found");
        die();

    }


    public static function success($message, $arr = array( )) {
        self::respond($message, 'OK', $arr);

    }


    public static function appendResult($message) {
        self::$messages[] = $message;

    }


    public static function failure($message, $arr = array( )) {
        self::respond($message, 'error', $arr);

    }


    /**
     * Return human readable sizes
     *
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.3.0
     * @link        http://aidanlister.com/2004/04/human-readable-file-sizes/
     * @param       int     $size        size in bytes
     * @param       string  $max         maximum unit
     * @param       string  $system      'si' for SI, 'bi' for binary prefixes
     * @param       string  $retstring   return string format
     */
    public static function getHumanSize($size, $max = null, $system = 'si', $retstring = '%01.2f %s') {
        // Pick units
        $systems['si']['prefix'] = array( 'B', 'kB', 'MB', 'GB', 'TB', 'PB' );
        $systems['si']['size']   = 1000;
        $systems['bi']['prefix'] = array( 'B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB' );
        $systems['bi']['size']   = 1024;
        $sys                     = isset($systems[$system]) ? $systems[$system] : $systems['si'];

        // Max unit to display
        $depth = count($sys['prefix']) - 1;
        if ( $max && false !== $d     = array_search($max, $sys['prefix']) ) {
            $depth = $d;
        }

        // Loop
        $i = 0;
        while ( $size >= $sys['size'] && $i < $depth ) {
            $size /= $sys['size'];
            $i++;
        }

        return sprintf($retstring, $size, $sys['prefix'][$i]);

    }


    /**
     * HELPER FUNCTIONS
     */

    /**
     * Parse values in multiline string like
     *
     * name1 => value1
     * name2 => value2
     *
     * into Array
     *
     * @param String $values
     * @param String $delimeter
     * @param String $comment
     * @return Array
     */
    public static function parseValues($values, $delimeter = '=>', $comment = '#') {
        $result = array( );
        foreach ( explode(PHP_EOL, $values) as $value ) {
            $value = trim($value);
            if ( !startsWith($value, $comment) && (!empty($value)) ) {
                $pos_delimeter = strpos($value, $delimeter);
                if ( $pos_delimeter > 1 ) {
                    $key          = trim(substr($value, 0, $pos_delimeter));
                    $val          = trim(substr($value, $pos_delimeter + strlen($delimeter)));
                    $val          = strlen($val) > 0 ? $val : $key;
                    $result[$key] = $val;
                } else {
                    $key          = trim($value);
                    $result[$key] = $key;
                }
            }
        };
        return $result;

    }


    /**
     * RETURN RELATIVE PATH $from FILE/DIRECTORY TO $to FILE/DIRECTORY:
     * default returns relative path from current page to root directory
     * 
     * @param type $to
     * @param type $from
     * @return type
     */
    public static function relPath($to = 0, $from = 0) {
        $from = self::pathFromRoot($from, 1);
        if ( $to ) {
            $basename = '/' . basename($to);
            $to       = self::pathFromRoot($to, 1);
        } else {//return path to root dir:
            return(str_repeat("../", count($from)));
        }

        //find point where paths branch by discarding mataching directories:
        for ( $i = 0; $i < count($from); $i++ ) {
            if ( $from[$i] == $to[$i] ) {
                unset($from[$i]);
                unset($to[$i]);
            }
        }
        //compile result:
        $result = str_repeat("../", count($from)); //write relative path to the branching point
        $result.=implode('/', $to); //write path to target direcory
        return($result . $basename);

    }


    /**
     * CONVERT A RELATIVE PATH TO AN ABSOLUTE PATH
     * 
     * @param type $relPathFromRoot
     * @param type $domain
     * @return type
     */
    public static function absPath($relPathFromRoot = 0, $domain = 0) {
        if ( !$relPathFromRoot ) {
            $relPathFromRoot = $_SERVER['PHP_SELF'];
        }//defaults to current page (calling script)
        if ( !$domain ) {
            $domain = $_SERVER['HTTP_HOST'];
        }//defaults to current domain
        return "http://" . $domain . str_replace(array( 'http://', 'www.', $domain ), '', $relPathFromRoot);

    }


    /**
     * RETURN STRING URL OR ARRAY OF DIRECTORIES FROM ROOT: dir1/dir2/etc OR array("dir1", "dir2", etc);
     * 
     * @param type $tofile
     * @param type $returnArray
     * @param type $remote
     * @return type
     */
    public static function pathFromRoot($tofile = 0, $returnArray = 0, $remote = 0) {
        $tofile = self::absPath($tofile);
        $path   = trim(self::parseUrl($tofile, 'path'), '/');
        return($returnArray ? explode('/', $path) : $path);

    }


    /**
     * RETURNS ARRAY OF COMPONANT PARTS OF AN ABSOLUTE URL
     * 
     * @param type $url
     * @param type $returnKey
     * @return type
     */
    public static function parseUrl($url, $returnKey = 0) {
        $r           = "^(?:(?P<scheme>\w+)://)?";
        $r .= "(?:(?P<login>\w+):(?P<pass>\w+)@)?";
        $r .= "(?P<host>(?:(?P<subdomain>[-\w\.]+)\.)?" . "(?P<domain>[-\w]+\.(?P<tld>\w+)))";
        $r .= "(?::(?P<port>\d+))?";
        $r .= "(?P<path>[\w/]*/(?P<file>\w+(?:\.\w+)?)?)?";
        $r .= "(?:\?(?P<arg>[\w=&]+))?";
        $r .= "(?:#(?P<anchor>\w+))?";
        $r           = "!$r!"; // Delimiters
        preg_match($r, $url, $out);
        $out['path'] = str_replace($out['file'], '', $out['path']); //hack fix
        return ($returnKey ? $out[$returnKey] : $out);

    }


    /**
     * Maskiert Sonderzeichen für den JavaScript-Kontext.
     * thanks to github user @dedlfix :)
     *
     * @param $value string Der zu behandelnde Wert.
     * @return string
     */
    static function jsEscape($value) {
        return strtr((string) $value, array(
                    "'"     => '\\\'',
                    '"'     => '\"',
                    '\\'    => '\\\\',
                    "\n"    => '\n',
                    "\r"    => '\r',
                    "\t"    => '\t',
                    chr(12) => '\f',
                    chr(11) => '\v',
                    chr(8)  => '\b',
                    '</'    => '\u003c\u002F',
        ));

    }

    /**
     * Checks if a given string contains PHP code
     * 
     * @param type $text
     * @return boolean
     */
    public static function hasPhpCode($text) {
        // WARNING!!! This is not guaranteed to be safe!!!
        // IF YOU FIND ANY VULNERABILITIES PLEASE LET ME KNOW
        try {
            $codeFound = FALSE;

            // SEARCHING FOR VARIANTS OF "script language=php" PHP opening tags
            $pattern1 = '#\<[\s]*script[\s]+lang.*=.*[\'"\s]*php[\s\'"]*\>#si';

            // SEARCHING FOR standard and short and ASP style PHP opening tags
            // omitting xml opening tags
            // only exact "<?xml" will pass (without space beetween ? and "xml")
            $pattern2 = '#\<(\?|%)(?!xml)#si';

            $result1 = preg_match($pattern1, $text);
            if ( ($result1 === 1) || // found occurence of long opening tag
                        ($result1 === FALSE) ) { // or search error occurred
                $codeFound = TRUE;
            }


            $result2 = preg_match($pattern2, $text);
            if ( ($result2 === 1) || // found occurence of standard/short opening tag
                        ($result2 === FALSE) ) { // or search error occurred
                $codeFound = TRUE;
            }
            return $codeFound;
        }
        catch ( Exception $exc ) { // something went wrong
            //echo $exc->getTraceAsString();
            return TRUE;   // so we assume the code was found!
        }

    }


}